package com.aswdc.matrimony;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class FirstActivity extends AppCompatActivity {
    Button btnsinig;
    TextView tvregisternow;
    EditText etEmail;
    EditText etPassword;

    boolean isValid() {
        boolean flag = true;
        //Email validation
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.lbl_enter_email_e));
            flag = false;
        } else {
            String email = etEmail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
            if (!(email.matches(emailPattern))) {
                etEmail.setError("Enter valid email address");
                flag = false;
                etEmail.requestFocus();
            }
            // Password validation
            if (TextUtils.isEmpty(etPassword.getText())) {
                etEmail.setError(getString(R.string.lbl_enter_password_ln));
                flag = false;
            } else {
                String password = etPassword.getText().toString().trim();
                String paswordpatten = "[a-zA-Z0-9]";
                if (!(password.matches(paswordpatten))) {
                    etPassword.setError("Enter valid password");
                    flag = false;
                    etPassword.requestFocus();
                }


            }
        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        etEmail = findViewById(R.id.etActEmail);
        btnsinig = findViewById(R.id.btnActsingin);
        tvregisternow = findViewById(R.id.tvActregisternow);


        btnsinig.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent homeIntent = new Intent(FirstActivity.this, HomeActivity.class);
                startActivity(homeIntent);

            }
        });
        tvregisternow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent registationIntent = new Intent(FirstActivity.this, RegistationActivity.class);
                startActivity(registationIntent);
            }
        });


    }
}

