package com.aswdc.matrimony;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button btnregistation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnregistation = findViewById(R.id.btnActregistation);


        btnregistation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent firstIntent = new Intent(MainActivity.this, FirstActivity.class);
                startActivity(firstIntent);
            }
        });
    }
}