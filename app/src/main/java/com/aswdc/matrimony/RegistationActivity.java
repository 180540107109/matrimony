package com.aswdc.matrimony;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class RegistationActivity extends AppCompatActivity {
    Button btnsubmit;

    EditText etFirstName;
    EditText etLastName;
    EditText etEmail;
    EditText etPhonenumber;
    EditText etPassword;
    EditText etConformPasswors;
    CheckBox chbbox;

    boolean isValid() {
        boolean flag = true;

        //Name validation
        if (TextUtils.isEmpty(etFirstName.getText())) {
            etFirstName.setError(getString(R.string.lbl_enter_FirstNmae_ln));
            flag = false;
        } else {
            String fName = etFirstName.getText().toString().trim();
            String fNamePattern = "[a-zA-Z]+\\.?";
            if (!(fName.matches(fNamePattern))) {
                etFirstName.setError("Enter valid Name");
                flag = false;
                etFirstName.requestFocus();
            }
        }
        if (TextUtils.isEmpty(etLastName.getText())) {
            etLastName.setError(getString(R.string.lbl_enter_Lastname_ln));
            flag = false;
        } else {
            String lName = etLastName.getText().toString().trim();
            String lNamePattern = "[a-zA-Z]+\\.?";
            if (!(lName.matches(lNamePattern))) {
                etLastName.setError("Enter valid Name");
                flag = false;
                etLastName.requestFocus();
            }
        }


        //Number validation
        if (TextUtils.isEmpty(etPhonenumber.getText())) {
            etPhonenumber.setError(getString(R.string.lbl_enter_phonenumber_ln));
            flag = false;
        } else {
            String phoneNumber = etPhonenumber.getText().toString();
            if (phoneNumber.length() < 10) {
                etPhonenumber.setError("Enter valid 10 digit number");
                flag = false;
                etPhonenumber.requestFocus();
            }
        }

        //Email validation
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.lbl_enter_email_e));
            flag = false;
        } else {
            String email = etEmail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
            if (!(email.matches(emailPattern))) {
                etEmail.setError("Enter valid email address");
                flag = false;
                etEmail.requestFocus();
            }
            // Password validation
            if (TextUtils.isEmpty(etPassword.getText())) {
                etEmail.setError(getString(R.string.lbl_enter_password_ln));
                flag = false;
            } else {
                String password = etPassword.getText().toString().trim();

                if (password.length() < 9) {
                    etPassword.setError("Enter valid password");
                    flag = false;
                    etPassword.requestFocus();
                }
            }
            // Password validation
            if (TextUtils.isEmpty(etConformPasswors.getText())) {
                etConformPasswors.setError(getString(R.string.lbl_enter_password_ln));
                flag = false;
            } else {
                String conformpassword = etConformPasswors.getText().toString().trim();

                if (!(conformpassword.equals(etPassword))) {
                    etConformPasswors.setError("password do not match");
                    flag = false;
                    etPassword.requestFocus();
                }

            }
            if (!(chbbox.isChecked())) {
                Toast.makeText(this, "Please select  checkbox", Toast.LENGTH_LONG).show();
                flag = false;
            }
        }
        return true;
    }
    /*
    if(isEmpty(RegisterConfirmPassword))
    {
        RegisterConfirmPassword.setError("Enter your confirmation password");

        if (!RegisterConfirmPassword.equals(RegisterPassword))
        {
            Toast.makeText(Signup.this, "Password do not match", Toast.LENGTH_SHORT).show();
        }
    }

     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registation);

        btnsubmit = findViewById(R.id.btnActSubmit);

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        etEmail = findViewById(R.id.etActEmail);
        etPhonenumber = findViewById(R.id.etActPhonenumber);
        etPassword = findViewById(R.id.etActPassword);
        etConformPasswors = findViewById(R.id.etActConformPassword);

        chbbox = findViewById(R.id.chbActbox);

        btnsubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent registationIntent = new Intent(RegistationActivity.this, HomeActivity.class);
                startActivity(registationIntent);
                if (isValid()) {
                    etPassword.requestFocus();
                    etPassword.requestFocus();
                    etEmail.requestFocus();
                    etPhonenumber.requestFocus();
                    etLastName.requestFocus();
                    etFirstName.requestFocus();
                }
            }
        });
    }


}